#!/usr/bin/env python
from pylabrat import PyLabRat
from random import random
from PyGMO import *
from PyKEP import *

class Rat(PyLabRat):

    def run(self, params):
        sequence = [planet_js("callisto"), planet_js("ganymede"), planet_js("ganymede"), planet_js("ganymede")]
        tof = [(180,200), (0.1, 5), (10, 150), (10,40)] 
        t0 = (epoch(10470-10), epoch(10470+10))
        orig_prob = problem.mga_incipit_cstrs(seq=sequence, tof=tof, t0=t0) #, Tmax=280)

        prob = eval(params["problem"])
        algo = eval(params["algorithm"])
        
        pop = population(prob, params["individuals"], int(random() * 1000))
        #print params["exp"], params["rep"], params["it"], pop.champion.f
        bestf_evo = []
        for i in xrange(params["evolutions"]):
            pop = algo.evolve(pop)
            bestf_evo.append(pop.champion.f[0])
#            print pop.champion.f[0]
            
        result = {}
        result["champion.f"] = pop.champion.f
        result["champion.x"] = pop.champion.x
        result["bestf_evo"] = bestf_evo

        result["label"] = str(pop.champion.f[0])
        return result

if __name__ == "__main__":
    rat = Rat()
    rat.start()
