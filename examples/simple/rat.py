#!/usr/bin/env python
from pylabrat import PyLabRat
from random import random

class Rat(PyLabRat):

    def run(self, params):
        result = {"random": random() * params["range"]}
        return result

if __name__ == "__main__":
    rat = Rat()
    rat.start()
