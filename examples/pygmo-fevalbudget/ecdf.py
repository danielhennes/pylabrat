#!/usr/bin/env python
from rat import Rat
import json
import matplotlib.pyplot as plt
import numpy as np

def get_cut_point(fevals, max_fevals=1e7):
    """ get cutting point to trim trajectory to max_fevals """
    nonz = (fevals >= max_fevals).nonzero()[0]
    if len(nonz) > 0:
        return min(nonz) -1
    else:
        return None

def cut_converged(fevals):
    last = 0
    for i, f in enumerate(fevals):
        if f == last:
            return i-1
        last = f
    return None

def mono_dec(x):
    """ transforms input to monotonically decreasing vector """
    for i in xrange(1, len(x)):
        if x[i] > x[i-1]:
            x[i] = x[i-1]
    return x

if __name__ == "__main__":
    rat = Rat()
    exps = rat.get_experiments()

    # target value
    ft = 0
    # target delta
    df = 1e-3
    # problem dimensions
    dim = 19

    feval_step = 1e4

    for exp in exps:
        print "-"*80
        print exp
        print "target: %f + %f"%(ft, df)

        results = rat.get_all_results(exp)[0]
        # filter None records
        results = [r for r in results if r is not None]

        F = []
        Fevals = []
        current_f = []
        current_fevals = []
        # assemable collumns

        print len(results)
        
        for r in results:
            # append r to current

            f = r["result"]["f"]
            fevals = np.array(r["result"]["fevals"])
            
            # cut when converged
            cut = cut_converged(fevals)
            if cut is not None:
                f = f[0:cut]
                fevals = fevals[0:cut]

            current_f.extend(f)

            if len(current_fevals) > 0:
                fevals = fevals + current_fevals[-1]
            current_fevals.extend(fevals)

            cut = get_cut_point(np.array(current_fevals))

            #print max(current_fevals), cut

            if cut is not None:
                current_f = mono_dec(current_f)
                F.append(current_f[0:cut])
                Fevals.append(current_fevals[0:cut])
                current_fevals = []
                current_f = []

        step  = Fevals[0][0]
        x = xrange(step+1,10000000+1,step)
#        x = xrange(25001,10000001,25000) # for 50
#        x = xrange(50001,10000001,50000) # for 50
        perc = []
        n = len(F)
#        n = 100
#        F = F[0:n]
       
        print n
        for i in x:
            count = 0
            for (f, feval) in zip(F, Fevals):
                # check if trail is converged by i-fevals
                feval = np.array(feval)
                f = np.array(f)
#                nonz = (feval < 1).nonzero()[0].
                trim = (feval <= i).nonzero()[0].argmax()
                if f[trim] < ft + df:
                    count += 1
            perc.extend([count/float(n)])
        
        plt.step(x, perc)
    plt.axes().set_xscale("log")
    plt.axes().set_ylim([0, 1])
    plt.show()

    # # plt.axis((0, max(feval), 0, 1))
    # # x0, x1 = plt.axes().get_xlim()
    # plt.axes().set_ylim([0, 1])
    # plt.axes().set_xlim([0, max(feval)])

    # # plt.axes().set_aspect((x1 - x0)/1)
    # plt.xlabel("log10 of FEvals") # / DIM")
    # plt.ylabel("percentage converged")

    # plt.grid(b=True, which='major', linestyle='--')

    # plt.legend(exps)
    # plt.show()

        
                

#        for i in range(len(F)):
#            plt.plot(Fevals[i], F[i])
#        plt.show()

#        print Fevals[5]

        # reindexing



#        F = np.array(F)
#        Fevals = np.array(Fevals)
#        print F.shape
#        print Fevals.shape

#        print np.max(Fevals, axis=1)
#        print Fevals
            

    #     f = np.array([r["result"]["f"] for r in results])
    #     fevals = np.array([r["result"]["fevals"] for r in results])
        
    #     for feval in fevals:
    #         print get_cut(feval), feval[get_cut(feval)]

    #     converged = (f < ft+df)
    #     perc_converged = np.array([np.count_nonzero(x) for x in converged.transpose()]) / float(f.shape[0])
    #     feval = np.mean(fevals, axis=0) # / dim

    #     # plt.plot(f.transpose())
    #     # plt.axes().set_xscale("log")
    #     # plt.show()
        
    #     plt.step(feval, perc_converged)
    # plt.axes().set_xscale("log")

    # # plt.axis((0, max(feval), 0, 1))
    # # x0, x1 = plt.axes().get_xlim()
    # plt.axes().set_ylim([0, 1])
    # plt.axes().set_xlim([0, max(feval)])

    # # plt.axes().set_aspect((x1 - x0)/1)
    # plt.xlabel("log10 of FEvals") # / DIM")
    # plt.ylabel("percentage converged")

    # plt.grid(b=True, which='major', linestyle='--')

    # plt.legend(exps)
    # plt.show()

