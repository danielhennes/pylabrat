#!/usr/bin/env python
from rat import Rat
import json
import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    rat = Rat() #no_cmd_line=True)
#    rat.load_config("experiments_20.json")
    exps = rat.get_experiments()

    # target value
    ft = 0
    # target delta
    df = 1e-3
    # problem dimensions
    dim = 19

    for exp in exps:
        print "-"*80
        print exp
        print "target: %f + %f"%(ft, df)

        results = rat.get_all_results(exp)[0]
        # filter
        results = [r for r in results if r is not None]

        f = np.array([r["result"]["f"] for r in results])
        fevals = np.array([r["result"]["fevals"] for r in results])
        
        converged = (f < ft+df)
        perc_converged = np.array([np.count_nonzero(x) for x in converged.transpose()]) / float(f.shape[0])
        feval = np.mean(fevals, axis=0) # / dim

        # plt.plot(f.transpose())
        # plt.axes().set_xscale("log")
        # plt.show()
        
        plt.step(feval, perc_converged)
    plt.axes().set_xscale("log")

    # plt.axis((0, max(feval), 0, 1))
    # x0, x1 = plt.axes().get_xlim()
    plt.axes().set_ylim([0, 1])
    plt.axes().set_xlim([0, max(feval)])

    # plt.axes().set_aspect((x1 - x0)/1)
    plt.xlabel("log10 of FEvals") # / DIM")
    plt.ylabel("percentage converged")

    plt.grid(b=True, which='major', linestyle='--')

    plt.legend(exps)
    plt.show()

