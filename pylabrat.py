#!/usr/bin/env python
from docopt import docopt
from copy import deepcopy
from multiprocessing import Process, Pool, cpu_count, Lock, Value, Manager
import json
import os
import sys
import shutil

class C:
    GREEN =  "\033[32m"
    YELLOW = "\033[33m"
    BLUE =   "\033[34m"
    PURPLE = "\033[35m"
    CYAN =   "\033[36m"
    ENDC =   "\033[0m"


def mp_do_it(args):
    (obj, params, lock, count) = args
    dic = PyLabRat.do_it(obj, params)
    result = dic["result"]
    params = dic["params"]
    label = result.get("label", "NO LABEL")
    lock.acquire()
    count.value += 1
    # output
    console_rows, console_cols = map(int, os.popen("stty size", "r").read().split())
    perc = 100 * (count.value + params["iterations"] * (params["rep"]-1)) / params["iterations"] / params["repetitions"]
    msg = "[%s%%] "%(repr(int(perc)).rjust(3))
    msg += C.GREEN + params["exp"] + C.ENDC + ": "
    msg += C.CYAN + label + C.ENDC
    progress = "[%s/%s] - [%s/%s]"%(
        repr(params["rep"]).rjust(len(str(params["repetitions"]))), 
        str(params["repetitions"]),
        repr(params["it"]).rjust(len(str(params["iterations"]))), 
        str(params["iterations"]))
    print msg + progress.rjust(console_cols - 9 - len(params["exp"]) - len(label))
    lock.release()
    return dic


class PyLabRat(object):

    _usage = """
Usage:
  %(name)s [-f FILE] [-j NUM]
  %(name)s clean [-f FILE]
  %(name)s --version

Options:
  -f FILE      Specify config file. [default: ./experiments.json]
  -j NUM       Specify the number of threads. [default: 1]
  -h --help    Show this screen.
  --version    Show version.

""" % ({"name": sys.argv[0]})

    def __init__(self, no_cmd_line=False):
        if no_cmd_line:
            return
        self._arguments = docopt(self._usage, version="PyRatLab 0.1")
        self._arguments["-j"] = int(self._arguments["-j"])
        self.load_config(self._arguments["-f"])
        if self._arguments["clean"]:
            if os.path.exists(self._global_params["path"]):
                shutil.rmtree(self._global_params["path"])
            sys.exit(0)
#        print "global:", json.dumps(self._global_params, indent=2)

    def run(self, params):
        """ needs to be implemented by subclass. """
        result = {}
        return result

    def start(self):
        for exp in self.get_experiments():
            # print "experiment: %s" % exp
            # create experiment dictionary
            params = deepcopy(self._global_params)
            params.update(self._experiments[exp])
                          # experiment parameter can overwrite defaults
            params["exp"] = exp
            self.do_exp(params)

    def do_exp(self, params):
        for r in xrange(1, params["repetitions"] + 1):
            params["rep"] = r
            self.do_rep(params)

    def mp_params(self, params, i):
        """ Helper method to copy params for multiprocessing. """
        p = deepcopy(params)
        p["it"] = i
        return p

    def do_rep(self, params):
        # create directory if necessary
        manager = Manager()
        lock = manager.Lock()
        count = manager.Value("i", 0)
        pool = Pool(processes=self._arguments["-j"])
        pool.map(mp_do_it, [(self, self.mp_params(params, i), lock, count) for i in xrange(1, params["iterations"] + 1)]) 
        pool.close()
        pool.join()

    def do_it(self, params):
        fname = self.get_fname(params)
        # if file exists we can skip this iteration
        dic = self.load_experiment(params)
        if dic is not None:
            return dic
        # if os.path.isfile(fname):
        #     return json.loads(open(fname).read())
        result = self.run(params)
        # save results with original params
        dic = {"params": params, "result": result}
        self.save_results(params, dic)
        return dic

    def save_results(self, params, dic):
        fname = self.get_fname(params)
        path, _ = os.path.split(fname)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(fname, mode="w") as f:
            json.dump(dic, f, indent=2)

    def get_fname(self, params):
        fname = ".".join([str(params["it"]).zfill(len(str(params["iterations"]))), "json"])
        fname = "/".join([
            params["path"], 
            params["exp"], 
            str(params["rep"]).zfill(len(str(params["repetitions"]))),
            fname])
        return fname

    def get_all_results(self, exp):
        params = deepcopy(self._global_params)
        params.update(self._experiments[exp])
        results = []
        for rep in xrange(1, params["repetitions"] + 1):
            results.append([])
            for it in xrange(1, params["iterations"] + 1):
                results[rep-1].append(self.get_results(exp, rep, it))
        return results

    def get_results(self, exp, rep, it):
        params = deepcopy(self._global_params)
        params.update(self._experiments[exp])
        params["exp"] = exp
        params["rep"] = rep
        params["it"] = it
        return self.load_experiment(params)

    def load_experiment(self, params):
        fname = self.get_fname(params)
        if os.path.isfile(fname):
            return json.loads(open(fname).read())
        else:
            return None

    def load_config(self, fname):
        config = json.loads(open(fname).read())
        self._global_params = config["global"]
        del config["global"]
        self._experiments = config

    def get_experiments(self):
        return self._experiments.keys()

if __name__ == "__main__":
    rat = PyLabRat()
    rat.start()
